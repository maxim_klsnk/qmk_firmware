#include QMK_KEYBOARD_H

#define LAYOUT_mirror( \
    L00, L01, L02, L03, L04, L05, \
    L10, L11, L12, L13, L14, L15, \
    L20, L21, L22, L23, L24, L25, L26, L27, \
                   L30, L31, L32, L33, L34  \
    ) \
    LAYOUT( \
        L00, L01, L02, L03, L04, L05,                     L05, L04, L03, L02, L01, L00, \
        L10, L11, L12, L13, L14, L15,                     L15, L14, L13, L12, L11, L10, \
        L20, L21, L22, L23, L24, L25, L26, L27, L27, L26, L25, L24, L23, L22, L21, L20, \
                       L30, L31, L32, L33, L34, L34, L33, L32, L31, L30 \
    )

enum LayoutNames {
    _MAIN,
    _GAME,
    _FN1,
    _FN2,
    _SPM
};

enum TapDanceActions {
    _STGL,
    _STGR,
    _STM
};

const keypos_t hand_swap_config[MATRIX_ROWS][MATRIX_COLS] =
{
    {{0, 4}, {1, 4}, {2, 4}, {3, 4}, {4, 4}, {5, 4}, {6, 4}, {7, 4}},
    {{0, 5}, {1, 5}, {2, 5}, {3, 5}, {4, 5}, {5, 5}, {6, 5}, {7, 5}},
    {{0, 6}, {1, 6}, {2, 6}, {3, 6}, {4, 6}, {5, 6}, {6, 6}, {7, 6}},
    {{0, 7}, {1, 7}, {2, 7}, {3, 7}, {4, 7}, {5, 7}, {6, 7}, {7, 7}},

    {{0, 0}, {1, 0}, {2, 0}, {3, 0}, {4, 0}, {5, 0}, {6, 0}, {7, 0}},
    {{0, 1}, {1, 1}, {2, 1}, {3, 1}, {4, 1}, {5, 1}, {6, 1}, {7, 1}},
    {{0, 2}, {1, 2}, {2, 2}, {3, 2}, {4, 2}, {5, 2}, {6, 2}, {7, 2}},
    {{0, 3}, {1, 3}, {2, 3}, {3, 3}, {4, 3}, {5, 3}, {6, 3}, {7, 3}},
};

qk_tap_dance_action_t tap_dance_actions[] = {
    [_STGL] = ACTION_TAP_DANCE_LAYER_MOVE(KC_LBRC, _GAME), // Switch to game layer on left side
    [_STGR] = ACTION_TAP_DANCE_LAYER_MOVE(KC_BSLS, _GAME), // Switch to game layer on right side
    [_STM] = ACTION_TAP_DANCE_LAYER_MOVE(KC_ENT, _MAIN) // Switch to main
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [_MAIN] = LAYOUT(
        LT(_SPM, KC_GRV), KC_Q, KC_W, KC_E,          KC_R,           KC_T,                                                                                    KC_Y,          KC_U,          KC_I,    KC_O,   KC_P,    LT(_SPM, KC_RBRC),
        KC_TAB,           KC_A, KC_S, KC_D,          KC_F,           KC_G,                                                                                    KC_H,          KC_J,          KC_K,    KC_L,   KC_SCLN, KC_QUOT,
        TD(_STGL),        KC_Z, KC_X, KC_C,          KC_V,           KC_B,          LT(_FN1, KC_SPC), LT(_FN2, KC_BSPC), LT(_FN2, KC_BSPC), LT(_FN1, KC_SPC), KC_N,          KC_M,          KC_COMM, KC_DOT, KC_SLSH, TD(_STGR),
                                      OSM(MOD_LGUI), OSM(MOD_LALT),  OSM(MOD_LSFT), OSM(MOD_LCTL),    SH_OS,            SH_OS,            OSM(MOD_LCTL),    OSM(MOD_LSFT), OSM(MOD_LALT), OSM(MOD_LGUI)
    ),

    [_GAME] = LAYOUT_mirror(
        KC_ESC,             KC_T, KC_Q, KC_G,    KC_E,    KC_R,
        KC_TAB,             KC_S, KC_A, KC_W,    KC_D,    KC_F,
        TD(_STM), KC_Z, KC_X, KC_C,    KC_V,    KC_B,    KC_SPC,  KC_H,
                                        KC_LGUI, KC_LALT, KC_LSFT, KC_LCTL, KC_M
    ),

    [_FN1] = LAYOUT(
        S(KC_9), KC_1, KC_2, KC_3,    KC_4,    S(KC_8),                                       _______, KC_F4, KC_F3,  KC_F2,  KC_F1, _______,
        KC_DOT,  KC_5, KC_6, KC_7,    KC_8,    S(KC_EQL),                                     _______, KC_F8, KC_F7,  KC_F6,  KC_F5, _______,
        S(KC_0), KC_9, KC_0, KC_MINS, KC_EQL,  S(KC_6),   _______, _______, _______, _______, _______, KC_F12, KC_F11, KC_F10, KC_F9, _______,
                             _______, _______,   _______, _______, _______, _______, _______, _______, _______, _______
    ),

    [_FN2] = LAYOUT(
        _______, _______, _______, _______, _______, _______,                                     _______, _______, _______, _______, _______, _______,
        _______, KC_DOWN, KC_LEFT, KC_UP,   KC_RGHT, _______,                                     KC_APP,  KC_ENT,  KC_INS,  KC_DEL,  KC_ESC,  _______,
        _______, KC_HOME, KC_PGUP, KC_END,  KC_PGDN, _______, _______, _______, _______, _______, _______, KC_PSCR, KC_SLCK, KC_PAUS, KC_CAPS, _______,
                                   _______, _______, _______, _______, _______, _______, _______, _______, _______, _______
    ),

    [_SPM] = LAYOUT_mirror( // Simple modifiers
        _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______,
                                   KC_LGUI, KC_LALT, KC_LSFT, KC_LCTL, _______
    )
};
