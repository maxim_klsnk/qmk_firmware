#include QMK_KEYBOARD_H

#define LAYOUT_ortho_4x12_mirrored( \
    L00, L01, L02, L03, L04, L05, \
    L10, L11, L12, L13, L14, L15, \
    L20, L21, L22, L23, L24, L25, \
    L30, L31, L32, L33, L34, L35 \
    ) \
    LAYOUT_ortho_4x12( \
        L00, L01, L02, L03, L04, L05, L05, L04, L03, L02, L01, L00, \
        L10, L11, L12, L13, L14, L15, L15, L14, L13, L12, L11, L10, \
        L20, L21, L22, L23, L24, L25, L25, L24, L23, L22, L21, L20, \
        L30, L31, L32, L33, L34, L35, L35, L34, L33, L32, L31, L30 \
    )

#define LAYOUT_ortho_4x12_left( \
    L00, L01, L02, L03, L04, L05, \
    L10, L11, L12, L13, L14, L15, \
    L20, L21, L22, L23, L24, L25, \
    L30, L31, L32, L33, L34, L35 \
    ) \
    LAYOUT_ortho_4x12( \
        L00, L01, L02, L03, L04, L05, _______, _______, _______, _______, _______, _______, \
        L10, L11, L12, L13, L14, L15, _______, _______, _______, _______, _______, _______, \
        L20, L21, L22, L23, L24, L25, _______, _______, _______, _______, _______, _______, \
        L30, L31, L32, L33, L34, L35, _______, _______, _______, _______, _______, _______ \
    )

#define LAYOUT_ortho_4x12_right( \
    L00, L01, L02, L03, L04, L05, \
    L10, L11, L12, L13, L14, L15, \
    L20, L21, L22, L23, L24, L25, \
    L30, L31, L32, L33, L34, L35 \
    ) \
    LAYOUT_ortho_4x12( \
        _______, _______, _______, _______, _______, _______, L00, L01, L02, L03, L04, L05, \
        _______, _______, _______, _______, _______, _______, L10, L11, L12, L13, L14, L15, \
        _______, _______, _______, _______, _______, _______, L20, L21, L22, L23, L24, L25, \
        _______, _______, _______, _______, _______, _______, L30, L31, L32, L33, L34, L35 \
    )


enum LayoutNames
{
    _MAIN,
    _GAME,
    _NUMBERS,
    _F_NUMBERS,
    _PARENTHESES,
    _SYMBOLS,
    _MEDIA,
    _OTHER,
    _UTILITY_LEFT,
    _UTILITY_RIGHT
};

enum TapDanceKeys
{
    TD_MAIN_LEFT,
    TD_MAIN_RIGHT,
    TD_GAME_LEFT,
    TD_GAME_RIGHT
};

qk_tap_dance_action_t tap_dance_actions[] = {
    [TD_MAIN_LEFT]  = ACTION_TAP_DANCE_LAYER_MOVE(KC_GRV, _MAIN),
    [TD_MAIN_RIGHT] = ACTION_TAP_DANCE_LAYER_MOVE(KC_RBRC, _MAIN),
    [TD_GAME_LEFT]  = ACTION_TAP_DANCE_LAYER_MOVE(KC_GRV, _GAME),
    [TD_GAME_RIGHT] = ACTION_TAP_DANCE_LAYER_MOVE(KC_RBRC, _GAME)
};

const keypos_t hand_swap_config[MATRIX_ROWS][MATRIX_COLS] =
{
	{{5, 5}, {4, 5}, {3, 5}, {2, 5}, {1, 5}, {0, 5}},
	{{5, 6}, {4, 6}, {3, 6}, {2, 6}, {1, 6}, {0, 6}},
	{{5, 7}, {4, 7}, {3, 7}, {2, 7}, {1, 7}, {0, 7}},
	{{5, 8}, {4, 8}, {3, 8}, {2, 8}, {1, 8}, {0, 8}},
	{{5, 9}, {4, 9}, {3, 9}, {2, 9}, {1, 9}, {0, 9}},

	{{5, 0}, {4, 0}, {3, 0}, {2, 0}, {1, 0}, {0, 0}},
	{{5, 1}, {4, 1}, {3, 1}, {2, 1}, {1, 1}, {0, 1}},
	{{5, 2}, {4, 2}, {3, 2}, {2, 2}, {1, 2}, {0, 2}},
	{{5, 3}, {4, 3}, {3, 3}, {2, 3}, {1, 3}, {0, 3}},
	{{5, 4}, {4, 4}, {3, 4}, {2, 4}, {1, 4}, {0, 4}}
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

    [_MAIN] = LAYOUT_ortho_4x12(
        TD(TD_GAME_LEFT),  KC_Q,               KC_W,                   KC_E,             KC_R,                    KC_T,                 KC_Y,                  KC_U,                     KC_I,             KC_O,                 KC_P,                 TD(TD_GAME_RIGHT),
        KC_TAB,            LT(_NUMBERS, KC_A), LT(_PARENTHESES, KC_S), LT(_MEDIA, KC_D), LT(_UTILITY_LEFT, KC_F), KC_G,                 KC_H,                  LT(_UTILITY_RIGHT, KC_J), LT(_OTHER, KC_K), LT(_SYMBOLS, KC_SPC), LT(_F_NUMBERS, KC_L), KC_SCLN,
        KC_LBRC,           KC_Z,               KC_X,                   KC_C,             KC_V,                    KC_B,                 KC_N,                  KC_M,                     KC_COMM,          KC_DOT,               KC_SLSH,              KC_QUOT,
        _______,           _______,            _______,                OSM(MOD_LGUI),    OSM(MOD_LCTL),           SH_MON,               SH_MON,                OSM(MOD_LSFT),            OSM(MOD_LALT),    _______,              _______,              _______
    ),

    [_GAME] = LAYOUT_ortho_4x12(
        TD(TD_MAIN_LEFT), KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    TD(TD_MAIN_RIGHT),
        KC_TAB,           KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_SPC,  KC_L,    KC_SCLN,
        KC_ESC,           KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, KC_QUOT,
        _______,          _______, _______, KC_LALT, KC_LSFT, KC_SPC,  KC_SPC,  KC_LCTL, KC_LGUI, _______, _______, _______
    ),

    [_NUMBERS] = LAYOUT_ortho_4x12_left(
        _______, _______, KC_1,    KC_2,    KC_3,    KC_4,
        _______, _______, KC_5,    KC_6,    KC_7,    KC_8,
        _______, _______, KC_9,    KC_0,    KC_MINS, KC_EQL,
        _______, _______, _______, _______, _______, _______
    ),

    [_F_NUMBERS] = LAYOUT_ortho_4x12_right(
        KC_F4,   KC_F3,   KC_F2,   KC_F1,   _______, _______,
        KC_F8,   KC_F7,   KC_F6,   KC_F5,   _______, _______,
        KC_F12,  KC_F11,  KC_F10,  KC_F9,   _______, _______,
        _______, _______, _______, _______, _______, _______
    ),

    [_PARENTHESES] = LAYOUT_ortho_4x12_left(
        _______, KC_LBRC, _______, S(KC_LBRC), S(KC_9), S(KC_COMM),
        _______, KC_RBRC, _______, S(KC_RBRC), S(KC_0), S(KC_DOT),
        _______, _______, _______, _______,    _______, _______,
        _______, _______, _______, _______,    _______, _______
    ),

    [_SYMBOLS] = LAYOUT_ortho_4x12_right(
        S(KC_4),   S(KC_3),    S(KC_2), _______, S(KC_1),    _______,
        S(KC_8),   S(KC_7),    S(KC_6), _______, S(KC_5),    _______,
        S(KC_EQL), S(KC_MINS), KC_BSLS,      _______, S(KC_BSLS), _______,
        _______,        _______,        _______,      _______, _______,    _______
    ),

    [_MEDIA] = LAYOUT_ortho_4x12_left(
        _______, KC_MSTP, KC_MPRV, _______, KC_VOLU, KC_BRIU,
        _______, KC_MPLY, KC_MNXT, _______, KC_VOLD, KC_BRID,
        _______, _______, _______, _______, KC_MUTE, KC_PWR,
        _______, _______, _______, KC_LGUI, KC_LCTL, _______
    ),

    [_OTHER] = LAYOUT_ortho_4x12_right(
        _______, S(A(KC_K)), _______, _______,    _______, _______,
        _______, S(A(KC_J)), _______, S(KC_LALT), _______, _______,
        _______, _______,    _______, _______,    _______, _______,
        _______, KC_LSFT, KC_LALT,    _______,    _______, _______
    ),

    [_UTILITY_LEFT] = LAYOUT_ortho_4x12_left(
        _______, KC_INS,  KC_HOME, KC_PGUP, _______, _______,
        _______, KC_ESC,  KC_END,  KC_PGDN, _______, _______,
        _______, KC_DEL,  KC_SLCK, KC_CAPS, _______, _______,
        _______, _______, _______, _______, _______, _______
    ),

    [_UTILITY_RIGHT] = LAYOUT_ortho_4x12_right(
        _______, _______, KC_UP,   KC_LEFT, KC_APP,  _______,
        _______, _______, KC_DOWN, KC_RGHT, KC_ENT,  _______,
        _______, _______, KC_PSCR, KC_PAUS, KC_BSPC, _______,
        _______, _______, _______, _______, _______, _______
    ),
};
