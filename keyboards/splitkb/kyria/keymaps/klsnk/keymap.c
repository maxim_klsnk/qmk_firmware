#include QMK_KEYBOARD_H

#define LAYOUT_mirror( \
    L00, L01, L02, L03, L04, L05, \
    L10, L11, L12, L13, L14, L15, \
    L20, L21, L22, L23, L24, L25, L26, L27, \
                   L30, L31, L32, L33, L34  \
    ) \
    LAYOUT( \
        L00, L01, L02, L03, L04, L05,                     L05, L04, L03, L02, L01, L00, \
        L10, L11, L12, L13, L14, L15,                     L15, L14, L13, L12, L11, L10, \
        L20, L21, L22, L23, L24, L25, L26, L27, L27, L26, L25, L24, L23, L22, L21, L20, \
                       L30, L31, L32, L33, L34, L34, L33, L32, L31, L30 \
    )

enum layout_names
{
    _MAIN,
    _GAME,
    _NUM,
    _SYM,
    _FUN,
    _NAV
};

enum tap_dance_actions
{
    TD_CB,
    TD_SB,
    TD_RB,
    TD_TB,
};

const keypos_t hand_swap_config[MATRIX_ROWS][MATRIX_COLS] =
{
    {{0, 4}, {1, 4}, {2, 4}, {3, 4}, {4, 4}, {5, 4}, {6, 4}, {7, 4}},
    {{0, 5}, {1, 5}, {2, 5}, {3, 5}, {4, 5}, {5, 5}, {6, 5}, {7, 5}},
    {{0, 6}, {1, 6}, {2, 6}, {3, 6}, {4, 6}, {5, 6}, {6, 6}, {7, 6}},
    {{0, 7}, {1, 7}, {2, 7}, {3, 7}, {4, 7}, {5, 7}, {6, 7}, {7, 7}},

    {{0, 0}, {1, 0}, {2, 0}, {3, 0}, {4, 0}, {5, 0}, {6, 0}, {7, 0}},
    {{0, 1}, {1, 1}, {2, 1}, {3, 1}, {4, 1}, {5, 1}, {6, 1}, {7, 1}},
    {{0, 2}, {1, 2}, {2, 2}, {3, 2}, {4, 2}, {5, 2}, {6, 2}, {7, 2}},
    {{0, 3}, {1, 3}, {2, 3}, {3, 3}, {4, 3}, {5, 3}, {6, 3}, {7, 3}},
};

qk_tap_dance_action_t tap_dance_actions[] = {
    [TD_CB] = ACTION_TAP_DANCE_DOUBLE(S(KC_LBRC), S(KC_RBRC)),
    [TD_SB] = ACTION_TAP_DANCE_DOUBLE(KC_LBRC, KC_RBRC),
    [TD_RB] = ACTION_TAP_DANCE_DOUBLE(S(KC_9), S(KC_0)),
    [TD_TB] = ACTION_TAP_DANCE_DOUBLE(S(KC_COMM), S(KC_DOT))
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [_MAIN] = LAYOUT(
        KC_GRV,  LT(_NUM, KC_Q), LT(_SYM, KC_W), LT(_NAV, KC_E),   LT(_FUN, KC_R), KC_T,                                                                      KC_Y,          LT(_FUN, KC_U), LT(_NAV, KC_I),    LT(_SYM, KC_O),   LT(_NUM, KC_P),    KC_BSLS,
        KC_TAB,  LT(_NUM, KC_A), LT(_SYM, KC_S), LT(_NAV, KC_D),   LT(_FUN, KC_F), KC_G,                                                                      KC_H,          LT(_FUN, KC_J), LT(_NAV, KC_K),    LT(_SYM, KC_L),   LT(_NUM, KC_SCLN), KC_QUOT,
        KC_LBRC, LT(_NUM, KC_Z), LT(_SYM, KC_X), LT(_NAV, KC_C),   LT(_FUN, KC_V), KC_B,          KC_SPC,        SH_OS,         SH_OS,         KC_SPC,        KC_N,          LT(_FUN, KC_M), LT(_NAV, KC_COMM), LT(_SYM, KC_DOT), LT(_NUM, KC_SLSH), KC_RBRC,
                                                 DF(_GAME),        OSM(MOD_LGUI),  OSM(MOD_LCTL), OSM(MOD_LSFT), OSM(MOD_LALT), OSM(MOD_LALT), OSM(MOD_LSFT), OSM(MOD_LCTL), OSM(MOD_LGUI),  DF(_GAME)
    ),

    [_GAME] = LAYOUT_mirror(
        KC_ESC, KC_T, KC_Q, KC_G, KC_E, KC_R,
        KC_TAB, KC_S, KC_A, KC_W, KC_D, KC_F,
        KC_ENT, KC_Z, KC_X, KC_C, KC_V, KC_B, KC_SPC, KC_H,
                       DF(_MAIN), KC_LGUI, KC_LCTL, KC_LSFT, KC_LALT
    ),

    [_NUM] = LAYOUT_mirror(
        _______, _______, KC_1, KC_2,    KC_3,     KC_4,
        _______, _______, KC_5, KC_6,    KC_7,     KC_8,
        _______, _______, KC_9, KC_0,    KC_MINUS, KC_EQL,  _______, _______,
                                _______, KC_LGUI,  KC_LCTL, KC_LSFT, KC_LALT
    ),

    [_SYM] = LAYOUT_mirror(
        _______, S(KC_1), _______, S(KC_2), S(KC_3),     S(KC_4),
        _______, S(KC_5), _______, S(KC_6), S(KC_7),     S(KC_8),
        _______, S(KC_9), _______, S(KC_0), S(KC_MINUS), S(KC_EQL), _______, _______,
                                   _______, _______,     _______,   _______, _______
),

    [_NAV] = LAYOUT(
        _______, KC_HOME,   KC_PGUP,   _______, KC_PGDN,   KC_END,                                      KC_HOME,   KC_PGDN,   _______, KC_PGUP,   KC_END,    _______,
        _______, KC_LEFT,   KC_UP,     _______, KC_DOWN,   KC_RIGHT,                                    KC_LEFT,   KC_DOWN,   _______, KC_UP,     KC_RIGHT,  _______,
        _______, TD(TD_CB), TD(TD_SB), _______, TD(TD_RB), TD(TD_TB), KC_ENT, KC_ESC,  KC_ESC,  KC_ENT, TD(TD_TB), TD(TD_RB), _______, TD(TD_SB), TD(TD_CB), _______,
                                       _______, KC_DEL,    KC_INS,    KC_APP, KC_BSPC, KC_BSPC, KC_APP, KC_INS,    KC_DEL,    _______
    ),

    [_FUN] = LAYOUT_mirror(
        KC_F4,  KC_F3,  KC_F2,   KC_F1,   _______, _______,
        KC_F8,  KC_F7,  KC_F6,   KC_F5,   _______, _______,
        KC_F12, KC_F11, KC_F10,  KC_F9,   _______, _______,  KC_LGUI, A(KC_LSFT),
                                 _______, _______, _______, _______, _______
    )
};
